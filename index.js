const express = require('express'),
      app = express(),
      http = require('http').Server(app),
      io = require('socket.io')(http),
      session = require('express-session'),
      bodyParser = require('body-parser'),
      cookieParser = require('cookie-parser'),
      port = process.env.port || 3000;


//cookie parser should used before express-session 
app.use(cookieParser());
app.use(bodyParser());
app.use(session({secret:'some-secret-token'}));


//handling user details for chat
app.post('/chat', function (req, res) {
  name = req.body.name;
  email = req.body.email;
  console.log(name + ' ' + email)
  if (name !== " " && email !== " ") {
    req.session.name = name;
    req.session.email = email;
  }
  res.redirect('/chat');
});

//chat page
app.get('/chat', function(req, res) {
  res.sendFile(__dirname + '/public/chat.html');
});  


//main page
app.get('/', function(req, res) {
  console.log('req for main page recieved');
  res.sendFile(__dirname + '/public/index.html');
});

//listening to port 3000
http.listen(port, function() {
  console.log('Started listening at port 3000');
});


//--------------------------------------------------
//socket handling

io.on('connection', function(socket) {
  console.log('A new client has connected');

});
